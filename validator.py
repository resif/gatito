#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from pathlib import Path

import jsonschema
import jsonref
import os
import logging

import yaml

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('validator')


def read_schema(schema_file_path):
    file = Path(schema_file_path)
    if not file.exists():
        raise Exception(f'Cannot find "{file.name}" schema file for validation')
    with open(schema_file_path, 'r') as f:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        base_uri = Path(os.path.join(current_dir, 'schemas')).as_uri()
        schema = jsonref.loads(f.read(), base_uri=base_uri, jsonschema=True)
    return schema


def validate(folder):
    files = [p for p in Path(folder).iterdir() if p.is_file()]
    for file in files:
        schema = read_schema(os.path.join('schemas', f'{Path(file).stem}.schema.json'))
        with open(file, 'r') as fl:
            valid_schema = jsonschema.Draft7Validator(schema)
            for error in valid_schema.iter_errors(yaml.safe_load(fl)):
                error_element = "".join(f"[{err}]" for err in error.path)
                logger.error(f"{file} has invalid file structure: {error_element} -> {error.message}")


def has_errors():
    return 40 in logger._cache and logger._cache[40]


if __name__ == '__main__':
    validate('demo')
    validate('resif')
    sys.exit(1) if has_errors() else print('All files are correct!')
