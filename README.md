# GATITO

**G**eneric **AT**omic l**I**brary of **T**iny **O**bjects : Library of metadata objects for solid earth science metadata, mostly enumerations

This library has been designed for Yasmine Tool.

There are two libraries in this repository

*  **demo** : used by Yasmine-gui for the demo version
*  **resif** : used for feed Seismological metadata of RESIF.

**GATITO Library elements**

1. **Station Comments**

A list of comments applicable to the station level

2.  **Channel Comments**

A list of comments applicable to the channel level

3.  **Geology**

A list of soil geology description

4.  **Vault**

A list of seismic vault or infrastructure descriptions

5.  **Operator**

A list of operators  
